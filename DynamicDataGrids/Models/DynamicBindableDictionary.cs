﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;

namespace DynamicDataGrids.Models
{
    public class DynamicBindableDictionary: DynamicObject, INotifyPropertyChanged
    {
        #region Class Fields
        private readonly Dictionary<string, object> _dictionary;
        #endregion

        #region Public Methods
        /// <summary>
        /// This is used to give the column names when the DataGrid ask for them in its autoGeneratingColumns event
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<string> GetDynamicMemberNames()
        {
            return _dictionary.Keys;
        }
        #endregion

        #region Constructor
        public DynamicBindableDictionary()
        {
            _dictionary = new Dictionary<string, object>();
        }

        public DynamicBindableDictionary(Dictionary<string, object> dictionary)
        {
            _dictionary = dictionary;
        }
        #endregion

        #region Dictionary Behavior
        public object this[string key]
        {
            get
            {
                return _dictionary[key];
            }
            set
            {
                _dictionary[key] = value;
                RaisePropertyChanged(key);
            }
        }
        #endregion

        #region DynamicObject Implementation
        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            return _dictionary.TryGetValue(binder.Name, out result);
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            _dictionary[binder.Name] = value;
            RaisePropertyChanged(binder.Name);
            return true;
        }
        #endregion

        #region INPC Implementation
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
