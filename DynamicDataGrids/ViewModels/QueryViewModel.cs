﻿using System.Collections.ObjectModel;
using System.Dynamic;
using System.Collections.Generic;
using DynamicDataGrids.Models;

namespace DynamicDataGrids.ViewModels
{
    public class QueryViewModel: BaseINPC
    {

        #region Properties
        private ObservableCollection<object> _resultData;

        public ObservableCollection<object> ResultData
        {
            get { return _resultData; }
            set
            {
                if (value != _resultData)
                {
                    _resultData = value;
                    RaisePropertyChanged("ResultData");
                }
            }
        }

        private void InitializeProperties()
        {

        }
        #endregion

        #region Constructor
        public QueryViewModel()
        {
            InitializeProperties();
            InitializeCommands();
            AddFakeColumnsToResultData();
        }
        #endregion

        #region Commands
        private void InitializeCommands()
        {

        }
        #endregion

        #region Private Methods
        private void AddFakeColumnsToResultData()
        {   
            var dd = new DynamicBindableDictionary();
            dd["Nombre"] = "Juan Carlos";
            dd["Apelldo"] = "Espinoza";
            dd["Edad"] = "21";
            dd["Direccion"] = "Dos Caminos, Villanueva";

            dynamic empleado = dd;

            ResultData = new ObservableCollection<object> {empleado};
        }
        #endregion
    }
}
