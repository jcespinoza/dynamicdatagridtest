﻿using System.Dynamic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Data;

namespace DynamicDataGrids.Views
{
    public partial class QueryView : UserControl
    {
        public QueryView()
        {
            InitializeComponent();
        }

        private void DynamicDataGrid_OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            var dataGrid = sender as DataGrid;

            var first = dataGrid.ItemsSource.Cast<object>().FirstOrDefault() as DynamicObject;
            if (first == null) return; //Make sure program does not stop, otherwise Josue will make fun of you

            var dynamicMemberNames = first.GetDynamicMemberNames();
            dataGrid.Columns.Clear(); //Not sure whybut DataGrid insists on Creating an "Item" column by default
            foreach (var memberName in dynamicMemberNames)
            {
                dataGrid.Columns.Add(
                    new DataGridTextColumn
                    {
                        Header = memberName, Binding = new Binding(memberName)
                    });
            }
        }
    }
}
